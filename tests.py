import unittest
import requests
import app as server


class CourseTestCase(unittest.TestCase):
    @classmethod
    def setUp(cls):
        cls._main_url = 'http://127.0.0.1:5000/api/v1/courses'
        cls._headers = {'Content-Type': 'application/json'}
        cls._correct_id = cls._get_correct_id()
        cls._wrong_id = cls._get_incorrect_id()

    @classmethod
    def _get_correct_id(cls):
        query = "SELECT CourseID FROM tblCourses ORDER BY RANDOM() LIMIT 1;"
        result = server.execute_sql_query(query)
        return str(result[0][0])

    @classmethod
    def _get_incorrect_id(cls):
        query = "SELECT CourseID + 1 FROM tblCourses ORDER BY CourseID desc LIMIT 1;"
        result = server.execute_sql_query(query)
        return str(result[0][0])

    @staticmethod
    def get_course_id(title, lectures, start_date, end_date):
        query = "SELECT CourseID FROM tblCourses WHERE Title = ? AND Lectures = ? AND StartDate = ? AND EndDate = ?"
        result = server.execute_sql_query(query, title, lectures, server.convert_date(start_date),
                                          server.convert_date(end_date))
        return result

    @staticmethod
    def get_course_data_by_id(course_id):
        query = "SELECT Title, Lectures, StartDate, EndDate  FROM tblCourses WHERE CourseID = ?"
        result = server.execute_sql_query(query, course_id)
        return result

    def test_add_course(self):
        title = "Added Test Course"
        lectures = 18
        start_date = "14:30 10/05/2019"
        end_date = "17:10 27/03/2020"
        body = {"Lectures": lectures,
                "Title": title,
                "StartDate": start_date,
                "EndDate": end_date}

        response = requests.post(self._main_url,  json=body)
        self.assertEqual(200, response.status_code)
        query = "SELECT CourseID FROM tblCourses WHERE Title = ? AND Lectures = ? AND StartDate = ? AND EndDate = ?"
        result = server.execute_sql_query(query, title, lectures, server.convert_date(start_date), server.convert_date(end_date))
        self.assertTrue(result)

    def test_add_course_with_empty_body(self):
        response = requests.post(self._main_url, json={})
        self.assertEqual(400, response.status_code)
        self.assertIn('Please check the body of the request', response.text)

    def test_add_course_with_missing_param(self):
        body = {"Lectures": "35",
                "StartDate": "14:30 10/05/2019",
                "EndDate": "17:10 27/03/2020"}
        response = requests.post(self._main_url, json=body)
        self.assertEqual(400, response.status_code)
        self.assertIn('Please check the body of the request', response.text)

    def test_add_course_with_wrong_date_format(self):
        # check start date format
        body = {"Lectures": "35",
                "Title": "Test Course 2",
                "StartDate": "14:30 10.05.2019",
                "EndDate": "17:10 27/03/2020"}
        response = requests.post(self._main_url, json=body)
        self.assertEqual(400, response.status_code)
        self.assertIn('Wrong date format', response.text)
        # check end date format
        body = {"Lectures": "35",
                "Title": "Test Course 2",
                "StartDate": "14:30 10/05/2019",
                "EndDate": "17:10 27.03.2020"}
        response = requests.post(self._main_url, json=body)
        self.assertEqual(400, response.status_code)
        self.assertIn('Wrong date format', response.text)

    def test_add_course_with_zero_duration(self):
        # start date and end date are the same
        body = {"Lectures": "35",
                "Title": "Test Course 2",
                "StartDate": "14:30 10/05/2019",
                "EndDate": "14:30 10/05/2019"}
        response = requests.post(self._main_url, json=body)
        self.assertEqual(400, response.status_code)
        self.assertIn('The course start date is equal or earlier than the end date', response.text)
        # end date is earlier than start date
        body = {"Lectures": "35",
                "Title": "Test Course 2",
                "StartDate": "17:15 10/05/2020",
                "EndDate": "14:30 10/05/2019"}
        response = requests.post(self._main_url, json=body)
        self.assertEqual(400, response.status_code)
        self.assertIn('The course start date is equal or earlier than the end date', response.text)

    def test_get_course_by_id(self):
        response = requests.get(self._main_url + '/' + self._correct_id)
        self.assertEqual(200, response.status_code)
        self.assertEqual(5, len(response.json()[0]))

    def test_get_course_with_wrong_id(self):
        response = requests.get(self._main_url + '/' + self._wrong_id)
        self.assertEqual(400, response.status_code)
        self.assertIn(f'The course with id {self._wrong_id} does not exist', response.text)

    def test_edit_course(self):
        title = "Edited Test Course"
        lectures = 22
        start_date = "12:00 25/04/2021"
        end_date = "14:00 25/05/2021"
        body = {"Lectures": lectures,
                "Title": title,
                "StartDate": start_date,
                "EndDate": end_date}

        response = requests.put(self._main_url + '/' + self._correct_id, json=body)
        self.assertEqual(200, response.status_code)
        self.assertIn('Done', response.text)
        result = self.get_course_data_by_id(self._correct_id)
        self.assertEqual(1, len(result))
        self.assertEqual(title, result[0][0])
        self.assertEqual(lectures, result[0][1])
        self.assertEqual(server.convert_date(start_date), result[0][2])
        self.assertEqual(server.convert_date(end_date), result[0][3])

    def test_edit_course_with_wrong_id(self):
        body = {"Lectures": "35",
                "Title": "Test Course 2",
                "StartDate": "14:30 10/05/2019",
                "EndDate": "17:10 27/03/2020"}

        response = requests.put(self._main_url + '/' + self._wrong_id, json=body)
        self.assertEqual(400, response.status_code)
        self.assertIn(f'The course with id {self._wrong_id} does not exist', response.text)

    def test_delete_course(self):
        response = requests.delete(self._main_url + '/' + self._correct_id)
        self.assertEqual(200, response.status_code)
        self.assertEqual('Done', response.text)
        result = self.get_course_data_by_id(self._correct_id)
        self.assertFalse(result)

    def test_delete_course_with_wrong_id(self):
        response = requests.delete(self._main_url + '/' + self._wrong_id)
        self.assertEqual(400, response.status_code)
        self.assertIn(f'The course with id {self._wrong_id} does not exist', response.text)


if __name__ == '__main__':
    unittest.main()
