from flask import Flask, request, jsonify, abort
import sqlite3
import datetime

app = Flask(__name__)

DB_FILE = "courses.db"
DATE_FORMAT = "%H:%M %d/%m/%Y"
MAIN_ROUTE = '/api/v1/courses'


# helper function to run sql command
def execute_sql_query(command, *args):
    conn = sqlite3.connect(DB_FILE)
    cursor = conn.cursor()
    if not args:
        cursor.execute(command)
    else:
        cursor.execute(command, args)
    conn.commit()
    return cursor.fetchall()


# helper function to convert date into unix timestamp
def convert_date(string_date):
    try:
        return datetime.datetime.strptime(string_date, DATE_FORMAT).timestamp()
    except ValueError:
        abort(400, 'Wrong date format. Correct format is: 14:30 15/03/2019')


# function to check if a course date range is correct
def compare_unix_dates(start_date, end_date):
    if start_date >= end_date:
        abort(400, 'The course start date is equal or earlier than the end date')


# a route to return all courses or filter by params
@app.route(MAIN_ROUTE, methods=['GET'])
def api_filter():
    if not request.args:
        query = """SELECT CourseID, Title, Lectures, strftime("%H:%M %d/%m/%Y", StartDate, "unixepoch", "localtime"), 
                          strftime("%H:%M %d/%m/%Y", EndDate, "unixepoch", "localtime") 
                   FROM tblCourses;"""
        results = execute_sql_query(query)
    else:
        query_parameters = request.args

        course_id = query_parameters.get('id')
        title = query_parameters.get('title')
        lectures = query_parameters.get('lectures')
        start_date = query_parameters.get('startDate')
        end_date = query_parameters.get('endDate')

        query = """SELECT CourseID, Title, Lectures, strftime("%H:%M %d/%m/%Y", StartDate, "unixepoch", "localtime"),
                          strftime("%H:%M %d/%m/%Y", EndDate, "unixepoch", "localtime") 
                   FROM tblCourses 
                   WHERE"""
        query_params = []
        if course_id:
            query += ' CourseID=? AND'
            query_params.append(course_id)
        if title:
            query += ' Title LIKE ? AND'
            query_params.append('%' + title + '%')
        if lectures:
            query += ' Lectures=? AND'
            query_params.append(lectures)
        if start_date:
            start_date_unix = convert_date(start_date)
            query += ' StartDate >= ? AND'
            query_params.append(start_date_unix)
        if end_date:
            end_date_unix = convert_date(end_date)
            query += ' EndDate <= ? AND'
            query_params.append(end_date_unix)

        query = query[:-4] + ';'
        results = execute_sql_query(query, *query_params)

    return jsonify(results)


# a route to return course info by id
@app.route(MAIN_ROUTE + '/<int:course_id>', methods=['GET'])
def api_id(course_id):
    query = """SELECT CourseID, Title, Lectures, strftime("%H:%M %d/%m/%Y", StartDate, "unixepoch", "localtime"),
                        strftime("%H:%M %d/%m/%Y", EndDate, "unixepoch", "localtime") 
               FROM tblCourses
               WHERE CourseID=?;"""
    results = execute_sql_query(query, course_id)
    if results:
        return jsonify(results)
    else:
        abort(400, f'The course with id {course_id} does not exist')


# a route to add new course
@app.route(MAIN_ROUTE, methods=['POST'])
def api_add():
    content = request.json
    if content.get('Title') and content.get('Lectures') and content.get('StartDate') and content.get('EndDate'):
        title = content['Title']
        lectures = content['Lectures']
        start_date_unix = convert_date(content['StartDate'])
        end_date_unix = convert_date(content['EndDate'])
        compare_unix_dates(start_date_unix, end_date_unix)

        query = """INSERT INTO tblCourses (Title, Lectures, StartDate, EndDate)
                   VALUES(?,?,?,?);"""
        execute_sql_query(query, title, lectures, start_date_unix, end_date_unix)
        return 'Done'
    else:
        abort(400, 'Please check the body of the request')


# a route to change data in existing course
@app.route(MAIN_ROUTE + '/<int:course_id>', methods=['PUT'])
def api_edit(course_id):
    api_id(course_id)
    content = request.json
    if content.get('Title') and content.get('Lectures') and content.get('StartDate') and content.get('EndDate'):
        title = content['Title']
        lectures = content['Lectures']
        start_date_unix = convert_date(content['StartDate'])
        end_date_unix = convert_date(content['EndDate'])
        compare_unix_dates(start_date_unix, end_date_unix)

        query = """UPDATE tblCourses SET Title = ?, Lectures= ?, StartDate = ?, EndDate = ?
                   WHERE CourseID = ?;"""
        execute_sql_query(query, title, lectures, start_date_unix, end_date_unix, course_id)
        return 'Done'
    else:
        abort(400, 'Request body is in wrong format')


# a route to delete a course
@app.route(MAIN_ROUTE + '/<int:course_id>', methods=['DELETE'])
def api_delete(course_id):
    api_id(course_id)
    query = """DELETE FROM tblCourses 
               WHERE CourseID = ?;"""
    execute_sql_query(query, course_id)

    return 'Done'


if __name__ == '__main__':
    app.run()
