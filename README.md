# The course catalog

Test project to manage course catalog over api

## Installation

Make sure that Python 3 is installed.
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install Flask.

```bash
pip install Flask
```

## Usage

To start the server run:
```bash
python3 app.py
```
To run the tests:
```bash
python3 tests.py
```
Note: The server should be up and running.


## API

Server host is: 
``` 
http://127.0.0.1:5000
```
Request to create new course:
```text
POST /api/v1/courses

        {
            "Title": "<title>",
            "StartDate": "<start date>",
            "EndDate": "<end date>",
            "Lectures": "<number of lectures>"
        }
```

Request to get all courses:
```text
GET /api/v1/courses
```

Request to get single course:
```text
GET /api/v1/courses/<id>
```

Request to get courses with filter applied:
```text
GET /api/v1/courses?title="<course title>"&
                    lectures="<number of lectures>"&
                    startDate="<filter by start date>"&
                    endDate="<filter by end date">
```

        
Request to change course:
```text
PUT /api/v1/courses/<id>
```

Request to delete course:
```text
DELETE /api/v1/courses/<id>
```

Note: Correct date format is: 
```
"%H:%M %d/%m/%Y" (For example: 19:10 27/03/2021)
```
All requests should contain in the header:
``` 
'Content-Type': 'application/json'
```